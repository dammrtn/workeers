Rails.application.routes.draw do
  resources :perks
  
  devise_for :users
  root	"pages#home"
  get "about" => "pages#about"
end