class AddAttachmentImageToPerks < ActiveRecord::Migration[5.0]
  def self.up
    change_table :perks do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :perks, :image
  end
end
