class AddPartnerNameOfferTitlePartnerDescriptionPartnerWebsitePartnerAreaPerkCodeToPerks < ActiveRecord::Migration[5.1]
  def change
    add_column :perks, :partner_name, :string
    add_column :perks, :offer_title, :string
    add_column :perks, :partner_description, :string
    add_column :perks, :partner_website, :string
    add_column :perks, :partner_area, :string
    add_column :perks, :perk_code, :string
  end
end
