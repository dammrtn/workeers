class AddAttachmentPartnerlogoToPerks < ActiveRecord::Migration[5.1]
  def self.up
    change_table :perks do |t|
      t.attachment :partnerlogo
    end
  end

  def self.down
    remove_attachment :perks, :partnerlogo
  end
end
