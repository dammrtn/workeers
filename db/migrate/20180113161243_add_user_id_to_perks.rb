class AddUserIdToPerks < ActiveRecord::Migration[5.1]
  def change
    add_column :perks, :user_id, :integer
    add_index :perks, :user_id
  end
end
