class AddAttachmentSmallimageToPerks < ActiveRecord::Migration[5.1]
  def self.up
    change_table :perks do |t|
      t.attachment :smallimage
    end
  end

  def self.down
    remove_attachment :perks, :smallimage
  end
end
