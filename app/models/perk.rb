class Perk < ApplicationRecord
	belongs_to :user

	has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
	has_attached_file :partnerlogo, styles: { medium: "300x300>", thumb: "100x100>" }
	has_attached_file :smallimage, styles: { medium: "300x300>", thumb: "100x100>" }
	validates_attachment_content_type :image, :partnerlogo, :smallimage, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
