class PerksController < ApplicationController
  before_action :set_perk, only: [:show, :edit, :update, :destroy]
  #before_action :correct_user, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  before_action :verify_is_admin, except: [:index, :show]

  def index
    @perks = Perk.all
  end

  def show
  end

  def new
    @perk = current_user.perks.build
  end

  def edit
  end

  def create
    @perk = current_user.perks.build(perk_params)
    if @perk.save
      redirect_to @perk, notice: 'Perk was successfully created.' 
    else
      render action: "new"
    end
  end

  def update
    if @perk.update(perk_params)
      redirect_to @perk, notice: 'Perk was successfully updated.' 
    else
      render action: "edit"
    end
  end

  def destroy
    @perk.destroy
    redirect_to perks_url, notice: 'Perk was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_perk
      @perk = Perk.find(params[:id])
    end

    def verify_is_admin
     (current_user.nil?) ? redirect_to(@perk) : (redirect_to(@perk) unless current_user.admin?)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def perk_params
      params.require(:perk).permit(:description, :image, :smallimage, :partnerlogo, :partner_name, :offer_title, :partner_description, :partner_website, :partner_area, :perk_code)
    end
end
